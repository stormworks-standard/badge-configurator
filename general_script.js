/* Stormworks Standard - Badge Configurator */

((global, $)=>{
  "use strict";

	let CONFIG = {}
	
    const CHECK_URL = 'https://sws.flaffipony.rocks/check?s='
    const BADGE_URL = 'https://badge.flaffipony.rocks/static/v1.svg?style=sws'

    const BADGE_PLACEHOLDER_URL = 'badge_placeholder.png'

	$(global).on('load', ()=>{		

		$.getJSON('/config.json', (data)=>{
            CONFIG = data
            global.sws.CONFIG = data
            log('CONFIG loaded!', CONFIG)
            window.dispatchEvent(new Event('sws_config_loaded'))
            global.sws.config_loaded = true
		})
    })

    function getStandardFromUrl(){
        let url = decodeURIComponent(document.location.href)
        let match = url.match(/[^\?]*\?s=(.*)/)
        let standard = match && match.length > 0 ? match[1] : ''
        return standard.replace(/\s+/g, ' ').trim()
    }

    function getParsedStandardFromUrl(){
        let ret = {}
        let standard = getStandardFromUrl()


        let _type = standard.substring(0,1)

        let _subtype = false
        if(standard.substring(1,1) !== ' ' && isNaN( parseInt( standard.substring(1,2) ) ) ){
            _subtype = standard.substring(1,2)
        }
             

        let _version
        if(_subtype){
            _version = standard.substring(2,3)
        } else {
            _version = standard.substring(1,2)
        }
        if(typeof _type !== 'string' || _version.length !== 1 ){
            ret.error = 'Type invalid'
            return ret
        }
        if(typeof _subtype === 'string' && _version.length !== 1 ){
            ret.error = 'Subtype invalid'
            return ret
        }
        if(typeof _version !== 'string' || _version.length === 0 || isNaN(parseInt(_version)) ){
            ret.error = 'Version invalid'
            return ret
        }
                
        
        let split = standard.split(' ')
        let _features = []
        if(split.length > 1){
            for(let i = 0; i < split[1].length; i++){
                _features.push(split[1].charAt(i))
            }
        }
        let _size
        if(split.length > 2){
            _size = split[2]
        }
        if(split.length > 3){
            ret.warning = 'Ignored additional text after the standard: ' + split[3] + ' ...'
        }

        ret.type = _type
        ret.subtype = _subtype
        ret.version = _version
        ret.features = _features
        ret.size = _size        

        return ret
    }

    function resetNotifications(){
        $('#error').fadeOut(200)
        $('#warning').fadeOut(200)
    }

    function notifyError(err){
        global.sws.log(err)
        $('#error').fadeIn(200).find('.message').html(err instanceof Error ? err.message : err.toString())
    }

    function notifyWarning(warn){
        global.sws.log(warn)
        $('#warning').fadeIn(200).find('.message').html(warn)
    }

    function log(){
        let args = ['SWS:']
        for(let a of arguments){
            args.push(a)
        }
		console.log.apply(console, args )
	}

    global.sws = {
        CONFIG: CONFIG,
        CHECK_URL: CHECK_URL,
        BADGE_URL: BADGE_URL,
        BADGE_PLACEHOLDER_URL: BADGE_PLACEHOLDER_URL,
        log: log,
        getStandardFromUrl: getStandardFromUrl,
        getParsedStandardFromUrl: getParsedStandardFromUrl,
        resetNotifications: resetNotifications,
        notifyError: notifyError,
        notifyWarning: notifyWarning
    }

})(window, jQuery)
