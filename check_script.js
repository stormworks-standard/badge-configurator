/* Stormworks Standard - Badge Configurator */

((global, $)=>{
  "use strict";

    let container
    let fulfilledRules
    

	$(global).on('load', ()=>{
        container = $('#container')
        fulfilledRules = container.find('#fulfilled-rules')
    
        if(global.sws.config_loaded === true){
            init()
        } else {
            global.addEventListener('sws_config_loaded', init)
        }        
    })

    function init(){
        
        buildBadge( global.sws.getStandardFromUrl() )
        buildPage( global.sws.getStandardFromUrl() )
    }

    function buildBadge(standard){
        $('#your-badge').attr('src', global.sws.BADGE_URL + '&message=' + encodeURIComponent(standard))
    }

    function buildPage(standard){
        global.sws.log('buildPage', standard)
        
        try {
            if(typeof standard !== 'string' || standard.length <= 1){
                throw new Error('Standard invalid')
            }
            
            let parsedStandards = global.sws.getParsedStandardFromUrl()
            if(parsedStandards.warning){
                global.sws.notifyWarning(parsedStandards.warning)
            }
            if(parsedStandards.error){
                throw new Error(parsedStandards.error)
            }
            buildRules(parsedStandards.type, parsedStandards.subtype, parsedStandards.version, parsedStandards.features, parsedStandards.size)
        } catch (err){
            global.sws.notifyError(err)
            fulfilledRules.html('')
        }
    }
    
    function buildRules(_type, _subtype, _version, _features, _size){
        global.sws.log('buildRules', _type, _subtype, _version, _features, _size)

        let information = {
            version: _version,
            size: _size
        }
       

        let typeFound = false
		for(let type of global.sws.CONFIG.types){
			if(type.typeshort === _type){
                typeFound = true
                information.type = type.type

                if(typeof _subtype !== 'string' || _subtype.length === 0){
                    if(type.versions.indexOf(_version) < 0){
                        throw new Error('Version does not exist for this type')
                    }                    
                } else {
                    let subtypeFound = false
                    for(let subtype of type.subtypes){
                        if(subtype.subtypeshort === _subtype){
                            subtypeFound = true
                            information.subtype = subtype.subtype
                            
                            if(subtype.versions.indexOf(_version) < 0){
                                throw new Error('Version does not exist for this type and subtype')
                            }	
                        }
                    }
                    if(!subtypeFound){
                        throw new Error('Subtype does not exist for this type')
                    }
                }

                let version = _version
                
                
                let features = global.sws.CONFIG.global_features
				if(type.features && type.features[version] && type.features instanceof Array){
					features = features.concat(type.features[version])
				}
				let rules = []
				if(type.rules && type.rules[version] && type.rules[version] instanceof Array){
					rules = rules.concat(type.rules[version])
				}
				let sizes = []
				if(type.sizes && type.sizes[version] && type.sizes[version] instanceof Array){
					sizes = sizes.concat(type.sizes[version])
				}


				if (typeof _subtype === 'string' && _subtype.length > 0) {
					for(let subtype of type.subtypes){
						if(subtype.subtypeshort === _subtype){			
							if(subtype.features instanceof Object && subtype.features[version] instanceof Array){
								features = features.concat(subtype.features[version])
							}
							if(subtype.rules instanceof Object && subtype.rules[version] instanceof Array){
								rules = rules.concat(subtype.rules[version])
							}
							if(subtype.sizes instanceof Object && subtype.sizes[version] instanceof Array){
								sizes = sizes.concat(subtype.sizes[version])
							}
						}
					}	
				}	

				
				for(let rule of rules){
					fulfilledRules.append('<div class="rule" type="rule">' + rule + '<span class="rule_hint">Type</span></div>')
				}

                let featuresFound = 0
                let featuresFoundChars = []
				for(let feature of features){
                    for(let _char of _features){
                        if(feature.char === _char){
                            let isAlreadyFound = false
                            for(let alreadyFound of featuresFoundChars){
                                if(alreadyFound === _char){
                                    isAlreadyFound = true
                                    break
                                }
                            }
                            if(!isAlreadyFound){
                                featuresFound++
                                featuresFoundChars.push(_char)
                                fulfilledRules.append('<div class="rule" type="feature">' + feature.rule + '<span class="rule_hint">Feature ' + feature.char + '</span></div>')
                            }
                        }
                    }
                }
                if(featuresFound !== _features.length){
                    global.sws.notifyWarning('Did not find all the features mentioned in the text. Only found ' + featuresFoundChars.join(','))
                }
                information.features = featuresFoundChars.join(',')
                
                let sizeFound = false
				for(let size of sizes){
                    if(size.label === _size){
                        sizeFound = true
                        fulfilledRules.append('<div class="rule" type="size">' + size.rule + '<span class="rule_hint">Size ' + size.label + '</span></div>')
                    }
                }
                if(typeof _size === 'string' && _size.length > 0 && sizeFound === false){
                    throw new Error('This size does not exist')
                }
			}
        }
        if(!typeFound){
            throw new Error('Type does not exist')
        }

        $('#information .type').html(information.type)
        $('#information .subtype').html(information.subtype)
        $('#information .version').html(information.version)
        $('#information .features').html(information.features)
        $('#information .size').html(information.size)
    }

    
	

})(window, jQuery)
