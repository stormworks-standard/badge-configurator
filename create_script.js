/* Stormworks Standard - Badge Configurator */

((global, $)=>{
  "use strict";

	
	let container
	let selectType
	let selectSubtype
	let selectVersion
	let selectSize
	let possibleFeatures
	let theRules
	let theSizes
	let allRules
	let badge

	let checkedFeatures = {}

	$(global).on('load', ()=>{
		container = $('#container')
		selectType = container.find('#select-type').on('change', ()=>{refreshContainer(true, true, true)})
		selectSubtype = container.find('#select-subtype').on('change', ()=>{refreshContainer(false, true, true)})
		selectVersion = container.find('#select-version').on('change', ()=>{refreshContainer(false, false, true)})
		selectSize = container.find('#select-size').on('change', ()=>{refreshBadge(); refreshAllRules()})
		possibleFeatures = container.find('#possible-features').on('change', ()=>{refreshBadge(); refreshAllRules()})
		theRules = container.find('#rules')
		theSizes = container.find('#sizes')
		allRules = container.find('#allrules')
		badge = container.find('#badge')

		if(global.sws.config_loaded === true){
            init()
        } else {
            global.addEventListener('sws_config_loaded', init)
        }
	})

	function init(){
		selectType.html('<option value="" selected></option>')
		for(let type of global.sws.CONFIG.types){
			selectType.append('<option value="' + type.typeshort + '">' + type.type + '</option>')
		}

		let standard = global.sws.getStandardFromUrl()
		if(typeof standard === 'string' && standard.length >= 2){
			let parsedStandards = global.sws.getParsedStandardFromUrl()
			
			if(parsedStandards.warning){
				global.sws.notifyWarning('Warning occured while parsing url param: ' + parsedStandards.warning)
			}

			if(parsedStandards.error){
				refreshContainer(true, true, true)
				global.sws.notifyError('Error occured while parsing url param: ' + parsedStandards.error)
			} else if(parsedStandards.type){
				selectType.find('option[value="' + parsedStandards.type + '"]').prop('selected', true)
				refreshContainer(true, true, true)
				setTimeout(()=>{
					if(parsedStandards.subtype){
						selectSubtype.find('option[value="' + parsedStandards.subtype + '"]').prop('selected', true)
						refreshContainer(false, true, true)
		
					}
					setTimeout(()=>{
						if(parsedStandards.version){
							selectVersion.find('option[value="' + parsedStandards.version + '"]').prop('selected', true)
							refreshContainer(false, false, true)								
							setTimeout(()=>{
								if(parsedStandards.size){
									selectSize.find('option[value="' + parsedStandards.size + '"]').prop('selected', true)			
								}
							},1)								
						}
					},1)						
				},1)		
			} else {
				refreshContainer(true, true, true)
			}

		} else {
			refreshContainer(true, true, true)
		}
	}

	function refreshAllRules(){
		global.sws.log('refreshing all rules')	
		global.sws.resetNotifications()

		allRules.find('.rule[type="size"], .rule[type="feature"]').remove()
		
		let activatedFeatures = []

		possibleFeatures.find('.feature').each((index, elem)=>{
			if($(elem).find('input').prop('checked')){
				activatedFeatures.push($(elem).find('.char').html())
				let newrule = $('<div class="rule" type="feature">' + $(elem).find('p').html() + '<span class="rule_hint">Feature ' + $(elem).find('.char').html() + '</span></div>')
				newrule.on('click', ()=>{
					newrule.toggleClass('checked')
				})
				allRules.append(newrule)
			}
		})

		theSizes.find('.size[size="' + selectSize.val() + '"]').each((i, elem)=>{
			let elemS = $(elem)

			if(elemS.attr('depends-on-feature') && activatedFeatures.indexOf(elemS.attr('depends-on-feature')) >= 0){

				let newrule = $('<div class="rule" type="size">' + elemS.find('p').html() + '<span class="rule_hint">Size ' +elemS.find('.label').html() + '</span></div>')
				newrule.on('click', ()=>{
					newrule.toggleClass('checked')
				})
				allRules.append(newrule)
			}
		})
	}

	function refreshContainer(changedType, changedSubtype, changedVersion){
		global.sws.log('refreshContainer', changedType, changedSubtype)
		global.sws.resetNotifications()

		if(changedType){
			selectSubtype.html('<option value="" selected></option>')
		}
		if(changedSubtype){
			selectVersion.html('<option value="" selected></option>')
		}
		if(changedVersion){
			selectSize.html('<option value="" selected></option>')
		}
		theRules.html('')
		theSizes.html('')
		possibleFeatures.html('')
		allRules.html('')

		for(let type of global.sws.CONFIG.types){
			if(type.typeshort === selectType.val()){
				selectSubtype.parent().show()
				selectSize.parent().show()
				selectVersion.parent().show()
				possibleFeatures.parent().show()
				theRules.parent().show()
				theSizes.parent().show()
				allRules.parent().show()
				$('#badge-container table').show()

				

				let version = selectVersion.val()
				
				if(typeof version !== 'string' || version.length === 0){
					if(changedType){
						version = type.versions[type.versions.length-1]
					}
					if(changedSubtype){
						for(let subtype of type.subtypes){
							if(subtype.subtypeshort === selectSubtype.val()){	
								version = subtype.versions[subtype.versions.length-1]		
							}
						}
					}
					selectVersion.find('option[value="' + version + '"]').prop('selected', 'true')
				}

				let features = global.sws.CONFIG.global_features
				if(type.features && type.features[version] && type.features[version] instanceof Array){
					features = features.concat(type.features[version])
				}
				let rules = []
				if(type.rules && type.rules[version] && type.rules[version] instanceof Array){
					rules = rules.concat(type.rules[version])
				}
				let sizes = []
				if(type.sizes && type.sizes[version] && type.sizes[version] instanceof Array){
					sizes = sizes.concat(type.sizes[version])
				}

				if(changedType){
					if(type.subtypes instanceof Array){
						for(let subtype of type.subtypes){
							selectSubtype.append('<option value="' + subtype.subtypeshort + '">' + subtype.subtype + '</option>')
						}
					}
				}
				
				if(changedType || changedSubtype){
					if (typeof selectSubtype.val() !== 'string' || selectSubtype.val().length === 0 ){
						if(type.versions instanceof Array){
							for(let v of type.versions){
								selectVersion.append('<option value="' + v + '" ' + (v===version? 'selected' : '') + '>' + v + '</option>')
							}
						}
					} else {
						for(let subtype of type.subtypes){
							if(subtype.subtypeshort === selectSubtype.val()){							
								if(subtype.versions instanceof Array){
									for(let v of subtype.versions){
										selectVersion.append('<option value="' + v + '" ' + (v===version? 'selected' : '') + '>' + v + '</option>')
									}
								}
							}
						}	
					}	
				}


				if (typeof selectSubtype.val() === 'string' && selectSubtype.val().length > 0) {
					for(let subtype of type.subtypes){
						if(subtype.subtypeshort === selectSubtype.val()){			
							if(subtype.features instanceof Object && subtype.features[version] instanceof Array){
								features = features.concat(subtype.features[version])
							}
							if(subtype.rules instanceof Object && subtype.rules[version] instanceof Array){
								rules = rules.concat(subtype.rules[version])
							}
							if(subtype.sizes instanceof Object && subtype.sizes[version] instanceof Array){
								sizes = sizes.concat(subtype.sizes[version])
							}
						}
					}	
				}	

				
				for(let rule of rules){
					theRules.append('<div class="rule">' + rule + '</div>')

					let newrule = $('<div class="rule" type="rule">' + rule + '<span class="rule_hint">Type</span></div>')
					newrule.on('click', ()=>{
						newrule.toggleClass('checked')
					})
					allRules.append(newrule)
				}

				for(let feature of features){
					let div = $('<div class="feature ' + (checkedFeatures[feature.char] === true ? 'checked' : '') + '" feature="' + feature.char + '"></div>')
					let checkbox = $('<input type="checkbox" char="' + feature.char + '" ' + (checkedFeatures[feature.char] === true ? 'checked' : '') + '/>')
						.on('change', ()=>{
							checkedFeatures[feature.char] = checkbox.prop('checked')
							$('.size[depends-on-feature="' + feature.char + '"]').css('display', checkbox.prop('checked')? 'flex' : 'none')
							selectSize.find('[depends-on-feature="' + feature.char + '"]').css('display', checkbox.prop('checked')? '' : 'none')
							refreshBadge();
							div.toggleClass('checked')
						})
					let char = $('<span class="char">' + feature.char + '</span>')
					let p = $('<p>' + feature.rule + '</p>')
					div.append(checkbox).append(char).append(p)
					div.on('click', ()=>{
						checkbox.prop('checked', !checkbox.prop('checked'))
						checkbox.change()
					})
					possibleFeatures.append(div)
				}
				

				for(let size of sizes){
					let div = $('<div class="size" size="' + size.label + '" ' + (size.depends_on_feature ? ('depends-on-feature="' + size.depends_on_feature + '" style="display: none"') : '') + '></div>')
					let label = $('<span class="label">' + size.label + '</span>')
					let p = $('<p>' + size.rule + '</p>')
					div.append(label).append(p)
					theSizes.append(div)

					selectSize.append('<option value="' + size.label + '" ' + (size.depends_on_feature ? ('depends-on-feature="' + size.depends_on_feature + '" style="display: none"') : '') + '>' + size.label + '</option>')
				}

				if(sizes.length === 0){
					theSizes.html('None')					
				}
				
				refreshBadge()
				refreshAllRules()
				return
			}
		}
		
		selectSubtype.parent().hide()
		selectVersion.parent().hide()
		selectSize.parent().hide()
		possibleFeatures.parent().hide()
		theRules.parent().hide()
		theSizes.parent().hide()
		allRules.parent().hide()
		$('#badge-container table').hide()
		clearBadge()
		refreshAllRules()
	}

	function clearBadge(){
		setBadge(false)
	}

	function refreshBadge(){
		setBadge(buildBadgeUrl())		
	}

	function setBadge(urlobject){
		global.sws.log('set badge url', urlobject)
		badge.html('')
		let url = urlobject.url
		let checkurl = urlobject.checkurl

		if(urlobject === false){
			url = global.sws.BADGE_PLACEHOLDER_URL
			badge.append('<object id="badge" type="image/svg+xml" data="' + url + '">  <img src="' + url + '"/> </object>')

			$('#the-image-url').html('')
			$('#the-html').html('')
			$('#the-workshop-markup').html('')
		} else {
			badge.append('<object id="badge" type="image/svg+xml" data="' + url + '">  <img src="' + url + '"/> </object>')

			$('#the-image-url').html(url)
			$('#the-html').html('<object id="badge" type="image/svg+xml" data="' + url + '">  <img src="' + url + '"/> </object>')
			$('#the-workshop-markup').html('[url=' + checkurl + '][img]' + url + '[/img][/url]')
		}		
	}

	function buildBadgeUrl(){
		let type = selectType.val()
		let subtype = selectSubtype.val()
		let version = selectVersion.val()
		let size = selectSize.val()
		let features = ""
		possibleFeatures.find('input[type="checkbox"]:checked').each((index, elem)=>{
			features += $(elem).attr('char')
		})

		let url = global.sws.BADGE_URL + '&message=' + encodeURIComponent(type + subtype + version)
		if(features){
			url += encodeURIComponent(' ' + features)
		}
		if(size){
			url += encodeURIComponent(' ' + size)
		}

		let checkurl = global.sws.CHECK_URL + encodeURIComponent(type + subtype + version)
		if(features){
			checkurl += encodeURIComponent(' ' + features)
		}
		if(size){
			checkurl += encodeURIComponent(' ' + size)
		}
		
		return {
			url: url,
			checkurl: checkurl
		}
	}	

})(window, jQuery)


function copyToClipboard(domselector){
	let dom = $(domselector)
	console.log('copying to clipboard', dom.html())
  	dom.get(0).select();
  	document.execCommand('copy');
}